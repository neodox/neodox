# The neodimio keymap for Neodox

This keymap use:

* quantum painter with a static logo, wpm counter, layer info in the master side;
* a rgb fader for smooth colours transitions ;
* rgb backlight that change colour based on the layer selected;
* a customized layout with tap dance enabled;
