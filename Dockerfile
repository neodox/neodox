FROM qmkfm/qmk_cli


WORKDIR /qmk_firmware

RUN git clone --recurse-submodules https://github.com/qmk/qmk_firmware.git && cd qmk_firmware && git checkout 0.22.11 && git submodule update --recursive
#these following step i thing they are not necessary
WORKDIR /qmk_firmware/qmk_firmware

